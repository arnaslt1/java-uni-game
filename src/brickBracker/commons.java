package brickBracker;

public interface commons {

    public static final int BACKGROUND_WIDTH = 692;
    public static final int BACKGROUND_HEIGHT = 592;
    public static final int BALL_START_X = 120;
    public static final int BALL_START_Y = 300;
    public static final int BALL_DIM = 20;
    public static final int PLAYER_START_X = 300;
    public static final int PLAYER_START_Y = 550;
    public static final int PLAYER_HEIGHT = 8;
    public static final int BRICK_WIDTH = 550/7;
    public static final int BRICK_HEIGHT = 50;

}
