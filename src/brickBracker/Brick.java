package brickBracker;


import java.awt.*;

public abstract class Brick extends Entity implements actInterface, commons {
    protected int health;
    protected boolean Alive;

    public Brick(int x, int y) {
        super(x, y, BRICK_WIDTH, BRICK_HEIGHT);
    }
    public abstract void isHit();
    public abstract void setHealth(int num);
    public abstract boolean isAlive();
    public abstract void checkIfAlive();
}
