
package brickBracker;

import java.awt.*;

public class BrickStrong extends Brick {

    public BrickStrong(int x, int y) {
        super(x, y);
        Alive = true;
        setHealth(3);
    }

    public void isHit() {
        health--;
    }

    public void setHealth(int num) {
        health = num;
    }

    public boolean isAlive() {
        if(!Alive){
            return false;
        }
        return true;
    }

    public void checkIfAlive(){
        if(health <= 0) {
            Alive = false;
        }
    }

    public void init(Graphics g) {
        g.setColor(Color.red);
        if(health <= 1){
            g.setColor(Color.red);
        }
        else if(health == 2){
            g.setColor(Color.green);

        }
        else if(health == 3){
            g.setColor(Color.yellow);

        }
        g.fillRect(myX,myY,myWidth,myHeight);
    }

}
