package brickBracker;

import java.awt.*;

public class Background extends Entity implements actInterface, commons {
    private boolean play = false;
    private int score = 0;

    public Background(){
        super(1,1,BACKGROUND_WIDTH,BACKGROUND_HEIGHT);
    }
    public void init(Graphics g){
        //background
        g.setColor(Color.black);
        g.fillRect(myX,myY,myWidth,myHeight);

        //borders
        g.setColor(Color.yellow);
        g.fillRect(0,0,3,592);
        g.fillRect(0,0,692,3);
        g.fillRect(691,0,3,592);

        //score
        g.setColor(Color.white);
        g.setFont(new Font("serif", Font.BOLD,25));
        g.drawString(""+ score,599,30);

    }

    public void setScore(){
        score+= 5;
    }
    public boolean playing() {
        return play;
    }
    public void setPlayingTrue (){
        play = true;
    }
    public void checkWonLost(int totalBricks, Ball b, Graphics g){
        if(totalBricks <= 0) {
            play = false;
            b.yDir = 0;
            b.xDir = 0;
            g.setColor(Color.RED);
            g.setFont(new Font("serif", Font.BOLD,30));
            g.drawString("You won :) " ,190,300);

            g.setFont(new Font("serif", Font.BOLD,20));
            g.drawString("Press Enter for new Game" ,230,350);

        }

        if(b.myY > 570){
            play = false;
            b.yDir = 0;
            b.xDir = 0;
            g.setColor(Color.RED);
            g.setFont(new Font("serif", Font.BOLD,30));
            g.drawString("Game Over, The Score: "+ score ,190,300);
            g.setFont(new Font("serif", Font.BOLD,20));
            g.drawString("Press Enter for new Game",230,350);

        }
    }
}
