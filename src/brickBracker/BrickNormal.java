
package brickBracker;

 import java.awt.*;

public class BrickNormal extends Brick {

    public BrickNormal(int x, int y) {
        super(x, y);
        Alive = true;
        setHealth(2);
    }

    public void isHit() {
        health--;
    }

    public void setHealth(int num) {
        health = num;
    }

    public boolean isAlive() {
        if(!Alive){
            return false;
        }
        return true;
    }

    public void checkIfAlive(){
        if(health <= 0) {
            Alive = false;
        }
    }

    public void init(Graphics g) {
        g.setColor(Color.green);
        if(health < 2){
            g.setColor(Color.red);
        }
        g.fillRect(myX,myY,myWidth,myHeight);
    }

}
