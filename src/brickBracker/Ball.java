package brickBracker;

import java.awt.*;

public class Ball extends Entity implements actInterface, commons {
    public int xDir = -1;
    public int yDir = -2;

    public Ball() {
        super(BALL_START_X,BALL_START_Y,BALL_DIM,BALL_DIM);
    }

    public void changeDirX(int numb){
        xDir = numb;
    }
    public void changeDirY(int numb){
        yDir = numb;
    }
    public void init(Graphics g) {
        //ball
        g.setColor(Color.white);
        g.fillOval(myX, myY, 20, 20);
    }
    public void move(){
        myX += xDir;
        myY += yDir;

        if(myX < 0) {
            xDir = -xDir;
        }
        if(myY < 0) {
            yDir = -yDir;
        }
        if(myX > 670) {
            xDir = -xDir;
        }
    }
    public void isHit() {
        yDir = -yDir;
    }


}
