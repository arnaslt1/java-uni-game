package brickBracker;

import java.awt.*;

public class Player extends Entity implements actInterface,commons  {
    public Player(int width){
        super(PLAYER_START_X,PLAYER_START_Y, width , commons.PLAYER_HEIGHT);
    }
    public void moveRight(int numb){
        myX += numb;
    }
    public void moveLeft(int numb){
        myX -= numb;
    }
    public void init(Graphics g) {
        g.setColor(Color.red);
        g.fillRect(myX, myY, myWidth, myHeight);
    }
}
