package brickBracker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Gameplay extends JPanel implements KeyListener, ActionListener {

    //pakuriu my variables
    private Player p = new Player(100);
    private Ball b = new Ball();
    private Background background = new Background();
    private int totalBricks = 7*1+7*2+7*3;
    //ball speed
    private Timer timer;
    private int delay = 8;

    private Generator bricks;

    public Gameplay() {
        bricks = new Generator(3,7);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(delay, this);
        timer.start();
    }
    public void paint(Graphics g) {
        //Entities
        background.init(g);
        p.init(g);
        b.init(g);
        //drawing bricks
        bricks.draw((Graphics2D)g);
        background.checkWonLost(totalBricks, b, g);
        g.dispose();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        timer.start();

        if(background.playing()){

            if(ballTouchesPlayer(b,p)){
                b.isHit();
            }

            BRICKS: for(int i = 0; i < bricks.map.length; i++){
                for(int j = 0; j< bricks.map[0].length;j++){
                    if(bricks.map[i][j].isAlive())
                    {
                        int brickX = j * bricks.brickWidth+80;
                        int brickY = i * bricks.brickHeight+50;
                        int brickWidth = bricks.brickWidth;
                        int brickHeight = bricks.brickHeight;

                        Rectangle brickRect = new Rectangle(brickX, brickY, brickWidth, brickHeight);
                        Rectangle ballRect = new Rectangle(b.myX,b.myY, 20,20);


                        if(ballRect.intersects(brickRect)){
                            bricks.setBrickValue(i,j);
                            totalBricks--;
                            background.setScore();

                            if(b.myX + 19 <= brickRect.x || b.myX + 1 >= brickRect.x+ brickRect.width){
                                b.xDir = -b.xDir;
                            }
                            else {
                                b.yDir = -b.yDir;
                            }
                            break BRICKS;
                        }
                    }
                }
            }
            b.move();
        }
        repaint();
    }

    public boolean ballTouchesPlayer (Ball b, Player p) {
        if(new Rectangle(b.myX, b.myY, b.myWidth, b.myHeight).intersects(new Rectangle(p.myX,p.myY,p.myWidth,p.myHeight)))
        {
            return true;
        }
        return false;
    }
    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyReleased(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            background.setPlayingTrue();
            if(p.myX >= 600){
                p.myX = 600;
            }
            else {
                p.moveRight(20);
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            background.setPlayingTrue();
            if(p.myX < 10){
                p.myX = 10;
            }
            else {
                p.moveLeft(20);
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            if(!background.playing()){
                //new game
                background = new Background ();
                background.setPlayingTrue();
                b = new Ball();
                p.myX = 310;
                totalBricks = 21;
                bricks = new Generator(3,7);
                repaint();

            }
        }
    }

}
