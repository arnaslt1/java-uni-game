package brickBracker;

import java.awt.*;

public abstract class Entity {

    protected int myX;
    protected int myY;
    protected int myWidth;
    protected int myHeight;


    public Entity (int x, int y , int width, int height)
    {
        myX = x;
        myY = y;
        myWidth = width;
        myHeight = height;

    }
}
