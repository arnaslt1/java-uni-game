package brickBracker;

import java.awt.*;

public class Generator {
    public Brick map[][]; //for bricks
    private BrickWeak brick = new BrickWeak(0,0);
    public int brickWidth = brick.myWidth;
    public int brickHeight = brick.myHeight;

    public Generator(int rows, int colummns) {
        map = new Brick[rows][colummns];
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map[0].length; j++){
                if(i == 0)
                    map[i][j] = new BrickStrong(j*brickWidth+80, i*brickHeight+50);

                if(i == 1)
                    map[i][j] = new BrickNormal(j*brickWidth+80, i*brickHeight+50);

                if(i == 2)
                    map[i][j] = new BrickWeak(j*brickWidth+80, i*brickHeight+50);




            }
        }
    }
    public void draw(Graphics2D g) {
        for(int i = 0; i < map.length; i++){
            for(int j = 0; j < map[0].length; j++){
                if(map[i][j].isAlive()){
                    map[i][j].init(g);
                    map[i][j].checkIfAlive();

                    g.setStroke(new BasicStroke(3));
                    g.setColor(Color.BLACK);
                    g.drawRect(j*brickWidth+80, i*brickHeight+50, brickWidth,brickHeight);

                }
            }
        }
    }
    public void setBrickValue(int row,int column){
        map[row][column].isHit();
    }

}